<!DOCTYPE html>
<html>
<head>
	<title>Rotocar - Szczeciński warsztat samochodowy</title>
	<meta charset="utf-8"/>
	<link rel="icon" href="mini-rotocar.png">
	<link rel="stylesheet" type="text/css" href="stylesheet.css">
</head>
<body>
	<header>
		<div id="header">
			<img src="mini-rotocar.png" id="mini" height="100%" width="2%" hspace="10" />
		</div>
	</header>
	<div id="main">
		<img src="logo-rotocar.png" id="logo" alt="logo roto-car" />
	</div>
	<div id="about">
	</div>
	<footer id="foot">
		<div id="f1"></div>
		<div id="f2"></div>
		<div id="f3"></div>
	</footer>
</body>
</html>
